package com.rule.drools.vo.resp;

import com.rule.drools.entity.ConditionInfoEntity;
import lombok.Data;

import java.util.List;

/**
 * @ClassName InfoListVo
 * @Description
 * @Author gz
 * @Date 2019/11/18 19:21
 * @Version 1.0
 */
@Data
public class InfoListVo {
    /**
     * 主键
     */
    private Long ruleId;
    /**
     * 场景
     */
    private Long sceneId;
    /**
     * 名称
     */
    private String ruleName;
    /**
     * 描述
     */
    private String ruleDesc;
    /**
     * 是否启用
     */
    private Integer ruleEnabled;
    /**
     * 是否有效
     */
    private Integer isEffect;
    /**
     * 属性ID
     */
    private Integer propertyId;
    /**
     * 备注
     */
    private String remark;

    /**
     * 规则条件
     */
    private List<ConditionInfoEntity> conditionInfoItem;
}
