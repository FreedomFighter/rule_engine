package com.rule.drools.vo.req;

import com.rule.drools.entity.ActionParamValueInfoEntity;
import lombok.Data;

import java.util.List;

/**
 * ClassName: ActionParamReqVo <br/>
 * Description: 动作参数<br/>
 * date: 2019/7/15 13:46<br/>
 *
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
public class ActionParamReqVo {
    /**
     * 动作id
	 */
    private Long actionId;
    /**
     * 参数名称
     */
    private String actionParamName;
    /**
     * 参数描述
     */
    private String actionParamDesc;
    /**
     * 标识
     */
    private String paramIdentify;
    /**
     * 是否有效
     */
    private Integer isEffect;
    /**
     * 备注
     */
    private String remark;
    /**
     * 参数值
     */
    private List<ActionParamValueInfoEntity> actionParamValueItem;

    private String paramValueStr;
}
