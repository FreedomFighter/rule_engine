package com.rule.drools.vo.resp;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName SceneListVo
 * @Description
 * @Author gz
 * @Date 2019/11/19 14:24
 * @Version 1.0
 */
@Data
public class SceneListVo {
    /**
     * 主键
     */
    private Long sceneId;
    /**
     * 标识
     */
    private String sceneIdentify;
    /**
     * 类型(暂不使用)
     */
    private Integer sceneType;
    /**
     * 名称
     */
    private String sceneName;
    /**
     * 描述
     */
    private String sceneDesc;
    /**
     * 是否有效
     */
    private Integer isEffect;
    /**
     * 创建人
     */
    private Long creUserId;
    /**
     * 创建时间
     */
    private Date creTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 关联实体
     */
    private String entityItem;
}
