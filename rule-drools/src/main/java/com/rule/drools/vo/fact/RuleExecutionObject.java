package com.rule.drools.vo.fact;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
public class RuleExecutionObject implements Serializable {

    //fact集合
    private List<Object> factObjectList = new ArrayList<Object>();

    //全局对象集合
    private Map<String, Object> globalMap = new HashMap<String, Object>();
    // 规则串
    private String rulesStr;

    /**
     * 功能描述:
     * 是否全部执行（默认全部）
     * @author : gz
     * @date : 2019/5/25  13:42
     */
    private boolean executeAll = true;

    /**
     * 功能描述:
     *〈根据名称过滤要执行的规则〉
     * @author : gz
     * @date : 2019/5/25  13:42
     */
    private String ruleName;

    /**
     * <p>
     * 方法说明: 添加fact对象
     *
     * @param factObject fact对象
     */
    public void addFactObject(Object factObject) {
        this.factObjectList.add(factObject);
    }

    /**
     * <p>
     * 方法说明: 设置Global参数
     *
     * @param key   key
     * @param value 值
     */
    public void setGlobal(String key, Object value) {
        this.globalMap.put(key, value);
    }
}
