package com.rule.drools.vo.req;

import lombok.Data;

import java.util.List;

/**
 * ClassName: ActionInfoReqVo <br/>
 * Description: 动作类 <br/>
 * date: 2019/7/15 11:58<br/>
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
public class ActionInfoReqVo {
    private Integer actionId;
    /**
     * 动作类型(1实现2自身)
     */
    private Integer actionType;
    /**
     * 动作名称
     */
    private String actionName;
    /**
     * 动作描述
     */
    private String actionDesc;
    /**
     * 动作实现类(包路径)
     */
    private String actionClass;
    /**
     * 是否有效
     */
    private Integer isEffect;
    /**
     * 备注
     */
    private String remark;
    /**
     * 动作参数
     */
    private List<ActionParamReqVo> actionParamItem;
}
