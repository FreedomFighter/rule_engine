package com.rule.drools.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TestRule extends BaseModel {
    private String message;
    private Integer amount;
    private Integer score;
}
