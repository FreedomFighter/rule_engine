package com.rule.drools.vo.req;

import lombok.Data;


/**
 * ClassName: RuleEntityItemReqVo <br/>
 * Description: <br/>
 * date: 2019/7/12 14:04<br/>
 *
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
public class RuleEntityItemReqVo {
    private Long id;

    /**
     * 字段名称
     */
    private String itemName;
    /**
     * 字段标识
     */
    private String itemIdentify;
    /**
     * 属性描述
     */
    private String itemDesc;

    /**
     * 是否有效
     */
    private String isEffect;
    /**
     * 备注
     */
    private String remark;
}
