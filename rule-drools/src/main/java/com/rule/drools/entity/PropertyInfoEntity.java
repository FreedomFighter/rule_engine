package com.rule.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 规则基础属性信息表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_property_info")
public class PropertyInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long rulePropertyId;
	/**
	 * 标识
	 */
	private String rulePropertyIdentify;
	/**
	 * 名称
	 */
	private String rulePropertyName;
	/**
	 * 描述
	 */
	private String rulePropertyDesc;
	/**
	 * 默认值
	 */
	private String defaultValue;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 备注
	 */
	private String remark;

}
