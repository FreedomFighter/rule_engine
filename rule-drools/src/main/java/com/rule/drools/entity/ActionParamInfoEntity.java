package com.rule.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 动作参数信息表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_action_param_info")
public class ActionParamInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long actionParamId;
	/**
	 * 动作id
	 */
	private Long actionId;
	/**
	 * 参数名称
	 */
	private String actionParamName;
	/**
	 * 参数描述
	 */
	private String actionParamDesc;
	/**
	 * 标识
	 */
	private String paramIdentify;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
