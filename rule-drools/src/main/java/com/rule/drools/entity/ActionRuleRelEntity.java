package com.rule.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 动作与规则信息关系表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_action_rule_rel")
public class ActionRuleRelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long ruleActionRelId;

	/**
	 * 动作与规则名称
	 */
	private String ruleActionRelName;
	/**
	 * 动作
	 */
	private Long actionId;
	/**
	 * 规则
	 */
	private Long ruleId;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
