package com.rule.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则动作定义信息
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_action_info")
public class ActionInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long actionId;
	/**
	 * 动作类型(1实现2自身)
	 */
	private Integer actionType;
	/**
	 * 动作名称
	 */
	private String actionName;
	/**
	 * 动作描述
	 */
	private String actionDesc;
	/**
	 * 动作实现类(包路径)
	 */
	private String actionClass;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;


	/**
	 * 获取实体标识(例如：TestRule  最后得到 testRule)
	 */
	public String getActionClazzIdentify() {
		int index = actionClass.lastIndexOf('.');
		return actionClass.substring(index + 1).substring(0, 1).toLowerCase() +
				actionClass.substring(index + 1).substring(1);
	}

}
