package com.rule.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 动作参数对应的属性值信息表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:39
 */
@Data
@TableName("rule_action_param_value_info")
public class ActionParamValueInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long actionParamValueId;
	/**
	 * 动作规则关系主键
	 */
	private Long ruleActionRelId;
	/**
	 * 动作参数
	 */
	private Long actionParamId;
	/**
	 * 参数值
	 */
	private String paramValue;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
