package com.rule.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import com.rule.drools.dao.VariableDao;
import com.rule.drools.entity.VariableEntity;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("variableService")
public class VariableService extends ServiceImpl<VariableDao, VariableEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<VariableEntity> page = this.page(
                new Query<VariableEntity>().getPage(params),
                new QueryWrapper<VariableEntity>()
        );

        return new PageUtils(page);
    }

}
