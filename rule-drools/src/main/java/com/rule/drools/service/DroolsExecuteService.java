package com.rule.drools.service;

import com.alibaba.fastjson.JSON;
import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.google.common.collect.Maps;
import com.rule.core.utils.CollectionUtil;
import com.rule.core.utils.Result;
import com.rule.drools.vo.fact.RuleExecutionObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName DroolsExecuteService
 * @Description 执行规则service
 * @Author gz
 * @Date 2019/11/19 17:49
 * @Version 1.0
 */
@Slf4j
@Service
public class DroolsExecuteService {
    @Autowired
    private SceneInfoService sceneInfoService;
    @CreateCache(cacheType= CacheType.LOCAL,expire = 3600)
    private Cache<String,Object> cache;
    @Autowired
    private DroolsRuleEngineService droolsRuleEngineService;

    /**
     * 执行规则
     * @param params
     * @return
     */
    public Result executeDrools(Map<String,Object> params){
        Map<String,Object> req =  (Map<String,Object>)params.get("params");
        String scene = req.get("scene").toString();
        RuleExecutionObject object = new RuleExecutionObject();
        Map<String,Object> map = Maps.newHashMap();
        List<Map<String,Object>> list = (List<Map<String,Object>>)req.get("conList");
        if(CollectionUtil.collectionIsNull(list)){
           return Result.error("请求参数为空");
        }
        for (Map<String, Object> objectMap : list) {
            map.put(objectMap.get("field").toString(),objectMap.get("fieldValue"));
        }
        log.info("请求参数:{}",map);
        object.addFactObject(map);
        Map<String,Object> result = new HashMap<>();
        object.setGlobal("_result",result);
        RuleExecutionObject objects= droolsRuleEngineService.excute(object,scene);
        log.info("执行结果:{}",JSON.toJSONString(objects));
        Object str = cache.get(scene);
        if(str != null){
            objects.setRulesStr(str.toString());
        }else {
            cache.put(scene,objects.getRulesStr());
        }
        return Result.ok().put("data",objects);
    }

}
