package com.rule.drools.service;

import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import com.rule.drools.dao.SceneEntityRelDao;
import com.rule.drools.entity.EntityInfoEntity;
import com.rule.drools.entity.SceneEntityRelEntity;
import com.rule.drools.entity.SceneInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("sceneEntityRelService")
public class SceneEntityRelService extends ServiceImpl<SceneEntityRelDao, SceneEntityRelEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SceneEntityRelEntity> page = this.page(
                new Query<SceneEntityRelEntity>().getPage(params),
                new QueryWrapper<SceneEntityRelEntity>()
        );

        return new PageUtils(page);
    }



    /**
     * <p>
     * 方法说明: 根据场景信息获取相关的实体信息
     *
     * @param baseRuleSceneInfo 参数
     */
  @Cached(name="entityInfoEntity_findBaseRuleEntityListByScene",cacheType= CacheType.LOCAL,key="#baseRuleSceneInfo.sceneIdentify", expire = 60)
  public  List<EntityInfoEntity> findBaseRuleEntityListByScene(SceneInfoEntity baseRuleSceneInfo){
      return this.baseMapper.findBaseRuleEntityListByScene(baseRuleSceneInfo);
  }

}
