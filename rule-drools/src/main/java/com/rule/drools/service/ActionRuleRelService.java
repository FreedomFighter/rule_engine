package com.rule.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import com.rule.drools.dao.ActionRuleRelDao;
import com.rule.drools.entity.ActionRuleRelEntity;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("actionRuleRelService")
public class ActionRuleRelService extends ServiceImpl<ActionRuleRelDao, ActionRuleRelEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ActionRuleRelEntity> page = this.page(
                new Query<ActionRuleRelEntity>().getPage(params),
                new QueryWrapper<ActionRuleRelEntity>()
        );

        return new PageUtils(page);
    }


    public void removeByRuleId(Long ruleId){
        LambdaUpdateWrapper<ActionRuleRelEntity> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(ActionRuleRelEntity::getRuleId,ruleId);
        this.remove(updateWrapper);
    }
}
