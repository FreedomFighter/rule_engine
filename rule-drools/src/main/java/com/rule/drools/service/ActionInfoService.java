package com.rule.drools.service;

import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.rule.core.exception.CustomException;
import com.rule.core.utils.CollectionUtil;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import com.rule.core.utils.Result;
import com.rule.drools.dao.ActionInfoDao;
import com.rule.drools.entity.ActionInfoEntity;
import com.rule.drools.entity.ActionParamInfoEntity;
import com.rule.drools.entity.ActionParamValueInfoEntity;
import com.rule.drools.entity.SceneInfoEntity;
import com.rule.drools.vo.req.ActionInfoReqVo;
import com.rule.drools.vo.req.ActionParamReqVo;
import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("actionInfoService")
public class ActionInfoService extends ServiceImpl<ActionInfoDao, ActionInfoEntity> {
    @Autowired
    private Mapper dozerMapper;
    @Autowired
    private ActionParamInfoService actionParamInfoService;
    @Autowired
    private ActionParamValueInfoService actionParamValueInfoService;

    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ActionInfoReqVo> page = this.baseMapper.pageAction(new Query<ActionInfoReqVo>().getPage(params));
        List<ActionInfoReqVo> records = page.getRecords();
        if(!CollectionUtil.collectionIsNull(records)){
            records.forEach(e->{
                List<ActionParamReqVo> actionParamItem = e.getActionParamItem();
                for (ActionParamReqVo vo : actionParamItem) {
                    List<String> collect = vo.getActionParamValueItem().stream().map(val -> val.getParamValue()).collect(Collectors.toList());
                    String join = StringUtils.join(collect, ',');
                    vo.setParamValueStr(join);
                }
            });
        }
        return new PageUtils(page);
    }

    /**
     * <p>
     * 方法说明: 获取动作列表
     *
     * @param baseRuleActionInfo 参数
     */
    public List<ActionInfoEntity> findBaseRuleActionInfoList(ActionInfoEntity baseRuleActionInfo){
       return  this.baseMapper.findBaseRuleActionInfoList(baseRuleActionInfo);
    }

    /**
     * <p>
     * 方法说明: 根据场景获取所有的动作信息
     *
     * @param sceneInfo 参数
     */
    @Cached(name="actionInfoEntity_findRuleActionListByScene",cacheType= CacheType.LOCAL, key="#sceneInfo.sceneIdentify", expire = 60)
    public List<ActionInfoEntity> findRuleActionListByScene(SceneInfoEntity sceneInfo){
        return  this.baseMapper.findRuleActionListByScene(sceneInfo);
    }


    /**
     * <p>
     * 方法说明: 根据规则id获取动作集合
     *
     * @param ruleId 参数
     */
    @Cached(name="actionInfoEntity_findRuleActionListByRule",cacheType= CacheType.LOCAL, key="#ruleId", expire = 60)
    public  List<ActionInfoEntity> findRuleActionListByRule( Long ruleId){
        return  this.baseMapper.findRuleActionListByRule(ruleId);
    }

    /**
     * <p>
     * 方法说明: 查询是否有实现类的动作
     *
     * @param ruleId 规则id
     */
    @Cached(name="actionInfoEntity_findRuleActionCountByRuleIdAndActionType",cacheType= CacheType.LOCAL, key="#ruleId", expire = 60)
    public Integer findRuleActionCountByRuleIdAndActionType( Long ruleId){
        return  this.baseMapper.findRuleActionCountByRuleIdAndActionType(ruleId);
    }
    /**
     * @Author gz
     * @Description 保存
     * @Date 2019/7/15 13:51
     * @Param reqVo 动作相关参数
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(ActionInfoReqVo reqVo){
        try {
            ActionInfoEntity map = dozerMapper.map(reqVo, ActionInfoEntity.class);
            // 保存动作信息
            this.save(map);
            // 动作参数
            List<ActionParamReqVo> actionParamItem = reqVo.getActionParamItem();
            if(CollectionUtil.collectionIsNull(actionParamItem)){
                throw new CustomException("动作参数不能为空");
            }
            actionParamItem.stream().forEach(actionParamReqVo -> {
                ActionParamInfoEntity actionParam = dozerMapper.map(actionParamReqVo, ActionParamInfoEntity.class);
                actionParam.setActionId(map.getActionId());
                // 保存动作参数
                boolean save = actionParamInfoService.save(actionParam);
                if(save){
                    List<ActionParamValueInfoEntity> valueItem = actionParamReqVo.getActionParamValueItem();
                    // 动作参数值
                    List<ActionParamValueInfoEntity> paramValue = valueItem.stream().map(entityVo -> {
                        entityVo.setActionParamId(actionParam.getActionParamId());
                        return entityVo;
                    }).collect(Collectors.toList());
                    // 保存动作参数值
                    actionParamValueInfoService.saveBatch(paramValue);
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
            throw new CustomException("保存失败!");
        }
        return Result.ok();
    }
    /**
     * @Author gz
     * @Description 获取动作名称，下拉选择
     * @Date 2019/7/15 15:41
     * @Param
     * @return Map<id,Name></>
     */
    public List<Map<String,Object>> getActionName(){
        List<ActionInfoEntity> list = this.list();
        List<Map<String,Object>> array = Lists.newArrayList();
        if(CollectionUtil.collectionIsNull(list)){
            return array;
        }
        for (ActionInfoEntity entity : list) {
            if(entity.getIsEffect() == 1){
                HashMap<String, Object> map = new HashMap<>(2);
                map.put("id",entity.getActionId());
                map.put("actName",entity.getActionName());
                array.add(map);
            }
        }
        return array;
    }

/**
 * @Author gz
 * @Description 通过动作名称获取单个信息
 * @Date 2019/7/16 10:11
 * @Param
 * @return
 */
    public ActionInfoReqVo getOne(Long actionId){
        return this.baseMapper.getOne(actionId);
    }


}
