package com.rule.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import com.rule.drools.dao.PropertyRelDao;
import com.rule.drools.entity.PropertyRelEntity;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("propertyRelService")
public class PropertyRelService extends ServiceImpl<PropertyRelDao, PropertyRelEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PropertyRelEntity> page = this.page(
                new Query<PropertyRelEntity>().getPage(params),
                new QueryWrapper<PropertyRelEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 通过ruleId删除
     * @param ruleId
     */
    public void removeByRuleId(Long ruleId){
        LambdaUpdateWrapper<PropertyRelEntity> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(PropertyRelEntity::getRuleId,ruleId);
        this.remove(updateWrapper);
    }
}
