package com.rule.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.core.exception.CustomException;
import com.rule.core.utils.CollectionUtil;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import com.rule.core.utils.Result;
import com.rule.drools.dao.SceneInfoDao;
import com.rule.drools.entity.SceneEntityRelEntity;
import com.rule.drools.entity.SceneInfoEntity;
import com.rule.drools.vo.req.RulesSceneEntitiyReqVo;
import com.rule.drools.vo.resp.SceneListVo;
import org.dozer.Mapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author gz
 * @Description 场景Service
 * @Date 2019/7/12 16:26
 * @since JDK 1.8
 */
@Service("sceneInfoService")
public class SceneInfoService extends ServiceImpl<SceneInfoDao, SceneInfoEntity> {
    @Autowired
    private Mapper dozerMapper;
    @Autowired
    private SceneEntityRelService sceneEntityRelService;

    /**
     * 分页获取场景列表
     *
     * @param params
     * @return
     */
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SceneListVo> page = this.baseMapper.pageScene(new Query<SceneListVo>().getPage(params));
        return new PageUtils(page);
    }

    /**
     * 方法描述：保存
     *
     * @param reqVo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(RulesSceneEntitiyReqVo reqVo) {
        try {
            SceneInfoEntity map = dozerMapper.map(reqVo, SceneInfoEntity.class);
            this.save(map);
            List<Long> entityId = reqVo.getEntityId();
            if (CollectionUtil.collectionIsNull(entityId)) {
                return Result.error("必须关联实体");
            }
            List<SceneEntityRelEntity> collect = entityId.stream().map(id -> {
                SceneEntityRelEntity relEntity = new SceneEntityRelEntity();
                relEntity.setEntityId(id);
                relEntity.setSceneId(map.getSceneId());
                return relEntity;
            }).collect(Collectors.toList());
            sceneEntityRelService.saveBatch(collect);
        } catch (MappingException e) {
            throw new CustomException("保存失败");
        }
        return Result.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result updateScene(RulesSceneEntitiyReqVo vo) {
        SceneInfoEntity map = dozerMapper.map(vo, SceneInfoEntity.class);
        this.updateById(map);
        List<Long> entityId = vo.getEntityId();
        if (!CollectionUtil.collectionIsNull(entityId)) {
            QueryWrapper<SceneEntityRelEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("scene_id", vo.getSceneId());
            sceneEntityRelService.remove(queryWrapper);
            List<SceneEntityRelEntity> collect = entityId.stream().map(id -> {
                SceneEntityRelEntity relEntity = new SceneEntityRelEntity();
                relEntity.setEntityId(id);
                relEntity.setSceneId(map.getSceneId());
                return relEntity;
            }).collect(Collectors.toList());
            sceneEntityRelService.saveBatch(collect);
        }
        return Result.ok();
    }

    /**
     * 方法描述：通过场景id获取场景信息
     *
     * @param id
     * @return
     */
    public RulesSceneEntitiyReqVo getOne(Long id) {
        return this.baseMapper.getOne(id);
    }

    /**
     * @return Map<Id , Name></>
     * @Author gz
     * @Description 获取场景名称
     * @Date 2019/7/15 15:48
     * @Param
     */
    public Map<Long, String> listSceneName() {
        List<SceneInfoEntity> list = this.list();
        if (CollectionUtil.collectionIsNull(list)) {
            return new HashMap<>();
        }
        Map<Long, String> map = list.stream().filter(sceneInfoEntity -> sceneInfoEntity.getIsEffect() == 1)
                .collect(Collectors.toMap(SceneInfoEntity::getSceneId, SceneInfoEntity::getSceneName));
        return map;
    }
}
