package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rule.drools.entity.PropertyRelEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 规则属性配置表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface PropertyRelDao extends BaseMapper<PropertyRelEntity> {
	
}
