package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rule.drools.entity.EntityInfoEntity;
import com.rule.drools.entity.SceneEntityRelEntity;
import com.rule.drools.entity.SceneInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 场景实体关联表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface SceneEntityRelDao extends BaseMapper<SceneEntityRelEntity> {

    /**
     * <p>
     * 方法说明: 查询场景与实体关系表信息
     *
     * @param baseRuleSceneEntityRelInfo 参数
     */
    List<SceneEntityRelEntity> findBaseRuleSceneEntityRelInfoList(SceneEntityRelEntity baseRuleSceneEntityRelInfo);

    /**
     * <p>
     * 方法说明: 根据场景信息获取相关的实体信息
     *
     * @param baseRuleSceneInfo 参数
     */
    List<EntityInfoEntity> findBaseRuleEntityListByScene(SceneInfoEntity baseRuleSceneInfo);
}
