package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rule.drools.entity.EntityInfoEntity;
import com.rule.drools.vo.req.RulesEntityReqVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 规则引擎实体信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface EntityInfoDao extends BaseMapper<EntityInfoEntity> {
    /**
     * <p>
     * 方法说明: 获取规则引擎实体信息
     *
     * @param baseRuleEntityInfo 参数
     */
    List<EntityInfoEntity> findBaseRuleEntityInfoList(EntityInfoEntity baseRuleEntityInfo);

    /**
     * <p>
     * 方法说明: 根据实体id获取实体信息
     *
     * @param id 实体id
     */
    EntityInfoEntity findBaseRuleEntityInfoById(@Param("id") Long id);

    /**
     * 分页获取实体信息
     * @param page
     * @return
     */
    IPage<RulesEntityReqVo> pageEntityInfo(IPage<RulesEntityReqVo> page);

    /**
     *  获取单个实体信息
     * @param entityId
     * @return
     */
    RulesEntityReqVo getOne(@Param("entityId") Long entityId);

}
