package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rule.drools.entity.SceneInfoEntity;
import com.rule.drools.vo.req.RulesSceneEntitiyReqVo;
import com.rule.drools.vo.resp.SceneListVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 规则引擎使用场景
 *
 * @author 360568523
 * @email gz@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface SceneInfoDao extends BaseMapper<SceneInfoEntity> {
    /**
     * 通过场景id获取单个场景信息
     * @param sceneId
     * @return
     */
    RulesSceneEntitiyReqVo getOne(@Param("sceneId") Long sceneId);


    IPage<SceneListVo> pageScene(IPage<SceneListVo> page);
}
