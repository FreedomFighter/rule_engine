package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rule.drools.entity.EntityItemInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 实体属性信息
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface EntityItemInfoDao extends BaseMapper<EntityItemInfoEntity> {


    /**
     * <p>
     * 方法说明: 根据实体id获取规则引擎实体属性信息
     *
     * @param baseRuleEntityItemInfo 参数
     */
    List<EntityItemInfoEntity> findBaseRuleEntityItemInfoList(EntityItemInfoEntity baseRuleEntityItemInfo);

    /**
     * <p>
     * 方法说明: 根据id获取对应的属性信息
     *
     * @param id 属性Id
     */
    EntityItemInfoEntity findBaseRuleEntityItemInfoById(final Long id);

}
