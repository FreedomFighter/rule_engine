package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rule.drools.entity.ConditionInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 规则条件信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:39
 */
@Mapper
public interface ConditionInfoDao extends BaseMapper<ConditionInfoEntity> {


    /**
     * <p>
     * 方法说明: 根据规则获取规则条件信息
     *
     * @param baseRuleConditionInfo 参数
     */
    List<ConditionInfoEntity> findBaseRuleConditionInfoList(ConditionInfoEntity baseRuleConditionInfo);

    /**
     * <p>
     * 方法说明: 根据规则id获取规则条件信息
     *
     * @param ruleId 规则id
     */
    List<ConditionInfoEntity> findRuleConditionInfoByRuleId(@Param("ruleId") Long ruleId);
}
