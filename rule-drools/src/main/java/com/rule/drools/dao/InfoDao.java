package com.rule.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rule.drools.entity.EntityItemInfoEntity;
import com.rule.drools.entity.InfoEntity;
import com.rule.drools.entity.PropertyInfoEntity;
import com.rule.drools.entity.SceneInfoEntity;
import com.rule.drools.vo.PropertyRelVo;
import com.rule.drools.vo.req.RulesInfoEntityReqVo;
import com.rule.drools.vo.resp.InfoListVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 规则信息
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface InfoDao extends BaseMapper<InfoEntity> {


    /**
     * Date 2017/7/25
     * Author lihao [lihao@sinosoft.com]
     * <p>
     * 方法说明: 获取规则集合信息
     *
     * @param baseRuleInfo 参数
     */
    List<InfoEntity> findBaseRuleInfoPage(InfoEntity baseRuleInfo);

    /**
     * Date 2017/7/25
     * Author lihao [lihao@sinosoft.com]
     * <p>
     * 方法说明: 查询规则属性信息
     *
     * @param baseRulePropertyInfo 参数
     */
    List<PropertyInfoEntity> findBaseRulePropertyInfoList(PropertyInfoEntity baseRulePropertyInfo);

    /**
     * Date 2017/7/25
     * Author lihao [lihao@sinosoft.com]
     * <p>
     * 方法说明: 根据规则获取已经配置的属性信息
     *
     * @param ruleId 参数
     */
    List<PropertyRelVo> findRulePropertyListByRuleId(@Param("ruleId") Long ruleId);

    /**
     * Date 2017/7/26
     * Author lihao [lihao@sinosoft.com]
     * <p>
     * 方法说明: 根据场景获取对应的规则规则信息
     *
     * @param baseRuleSceneInfo 参数
     */
    List<InfoEntity> findBaseRuleListByScene(SceneInfoEntity baseRuleSceneInfo);

    /**
     * 通过规则id获取单个
     * @param ruleId
     * @return
     */
    RulesInfoEntityReqVo getOne(Long ruleId);

    IPage<InfoListVo> pageInfoList(IPage<InfoListVo> page, Map<String, Object> params);


    /**
     * 获取条件实体
     * @param scene
     * @return
     */
    List<EntityItemInfoEntity> execRuleConditionReq(String scene);

    /**
     * 获取结果实体
     * @param scene
     * @return
     */
    List<EntityItemInfoEntity> execRuleResultReq(String scene);
}
