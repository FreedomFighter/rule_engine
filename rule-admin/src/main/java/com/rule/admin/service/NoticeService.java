package com.rule.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.admin.dao.NoticeDao;
import com.rule.admin.entity.NoticeEntity;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class NoticeService extends ServiceImpl<NoticeDao, NoticeEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<NoticeEntity> page = this.page(
                new Query<NoticeEntity>().getPage(params),
                new QueryWrapper<NoticeEntity>()
        );

        return new PageUtils(page);
    }

}
