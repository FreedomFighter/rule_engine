package com.rule.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.admin.dao.DictDao;
import com.rule.admin.entity.DictEntity;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DictService extends ServiceImpl<DictDao, DictEntity> {

  public PageUtils queryPage(Map<String, Object> params) {

    DictEntity dict = new DictEntity();
    String key=String.valueOf(params.get("key"));
    if (StringUtils.isNotBlank(key)) {
      dict.setType(key);
    }
    IPage<DictEntity> page =
        this.page(new Query<DictEntity>().getPage(params), new QueryWrapper<DictEntity>(dict));

    return new PageUtils(page);
  }
}
