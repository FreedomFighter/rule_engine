package com.rule.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rule.admin.dao.MessagePushDao;
import com.rule.admin.entity.MessagePushEntity;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class MessagePushService extends ServiceImpl<MessagePushDao, MessagePushEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MessagePushEntity> page = this.page(
                new Query<MessagePushEntity>().getPage(params),
                new QueryWrapper<MessagePushEntity>()
        );

        return new PageUtils(page);
    }

}
