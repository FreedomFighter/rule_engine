package com.rule.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.rule.admin.entity.SysLogEntity;
import com.rule.core.utils.PageUtils;

import java.util.Map;


/**
 * 系统日志
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
