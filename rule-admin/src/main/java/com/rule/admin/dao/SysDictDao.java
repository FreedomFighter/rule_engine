package com.rule.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rule.admin.entity.SysDictEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {

}
