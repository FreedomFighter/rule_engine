package com.rule.admin.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rule.admin.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {

}
