package com.rule.front.api.drools;

import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Result;
import com.rule.drools.entity.InfoEntity;
import com.rule.drools.service.InfoService;
import com.rule.drools.vo.req.RulesInfoEntityReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 规则信息
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@RestController
@RequestMapping("rules/info")
public class InfoController {
    @Autowired
    private InfoService infoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = infoService.queryPage(params);
        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{ruleId}")
    public Result info(@PathVariable("ruleId") Long ruleId){
        RulesInfoEntityReqVo info = infoService.getOne(ruleId);
        return Result.ok().put("info", info);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody RulesInfoEntityReqVo info){
	    return infoService.save(info);
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody InfoEntity info){
		infoService.updateById(info);

        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] ruleIds){
		infoService.removeByIds(Arrays.asList(ruleIds));

        return Result.ok();
    }

    /**
     * 启用/停用
     * @param ruleId
     * @return
     */
    @RequestMapping("/startOrStop")
    public Result startOrStop(@RequestParam(value = "ruleId") Long ruleId,@RequestParam(value = "enable") Integer enable){
        InfoEntity entity = infoService.getById(ruleId);
        if(entity==null){
            return Result.error("数据不存在");
        }
        entity.setRuleEnabled(enable);
        infoService.updateById(entity);
        return Result.ok();
    }

}
