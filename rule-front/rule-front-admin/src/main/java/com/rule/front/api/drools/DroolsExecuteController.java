package com.rule.front.api.drools;

import com.google.common.collect.Lists;
import com.rule.core.utils.Result;
import com.rule.drools.entity.EntityItemInfoEntity;
import com.rule.drools.service.DroolsExecuteService;
import com.rule.drools.service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName DroolsController
 * @Description
 * @Author gz
 * @Date 2019/11/19 17:39
 * @Version 1.0
 */
@RestController
@RequestMapping(value = "drools")
public class DroolsExecuteController {
    @Resource
    private DroolsExecuteService droolsEngineService;
    @Autowired
    private InfoService infoService;

    /**
     * 通过场景标识执行规则文件
     * @param params
     * @return
     */
    @RequestMapping(value = "executeDrools")
    public Result executeDrools(@RequestBody Map<String,Object> params) {
        return droolsEngineService.executeDrools(params);
    }

    /**
     * 通过场景获取规则执行参数
     * @param scene
     * @return
     */
    @GetMapping(value = "execRuleParam")
    public Result execRuleParam(@RequestParam String scene){
        // 条件集
        List<EntityItemInfoEntity> conditionReq = infoService.getBaseMapper().execRuleConditionReq(scene);
        // 结果集
        List<EntityItemInfoEntity> resultReq = infoService.getBaseMapper().execRuleResultReq(scene);
        conditionReq.addAll(resultReq);
        List<EntityItemInfoEntity> collect = conditionReq.stream().distinct().collect(Collectors.toList());
        List<Map<String,Object>> array = Lists.newArrayList();
        for (EntityItemInfoEntity vo : collect) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("field",vo.getItemIdentify());
            map.put("fieldName",vo.getItemName());
            map.put("fieldValue",null);
            array.add(map);
        }
        return Result.ok().put("con",array);
    }
}
