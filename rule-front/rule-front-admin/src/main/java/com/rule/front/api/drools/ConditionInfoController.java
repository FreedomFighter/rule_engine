package com.rule.front.api.drools;

import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Result;
import com.rule.drools.entity.ConditionInfoEntity;
import com.rule.drools.service.ConditionInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 规则条件信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:39
 */
@RestController
@RequestMapping("rules/conditioninfo")
public class ConditionInfoController {
    @Autowired
    private ConditionInfoService conditionInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = conditionInfoService.queryPage(params);
        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{conditionId}")
    public Result info(@PathVariable("conditionId") Long conditionId){
		ConditionInfoEntity conditionInfo = conditionInfoService.getById(conditionId);

        return Result.ok().put("conditionInfo", conditionInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody ConditionInfoEntity conditionInfo){
		conditionInfoService.save(conditionInfo);

        return Result.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody ConditionInfoEntity conditionInfo){
		conditionInfoService.updateById(conditionInfo);

        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] conditionIds){
		conditionInfoService.removeByIds(Arrays.asList(conditionIds));

        return Result.ok();
    }

}
