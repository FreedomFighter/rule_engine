package com.rule.front.api.admin;

import com.rule.admin.entity.MessagePushEntity;
import com.rule.admin.service.MessagePushService;
import com.rule.admin.service.MessagePushService;
import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 系统消息推送
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-22 14:19:18
 */
@RestController
@RequestMapping("sys/messagepush")
public class MessagePushController {
    @Autowired
    private MessagePushService messagePushService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = messagePushService.queryPage(params);

        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Result info(@PathVariable("id") String id){
		MessagePushEntity messagePush = messagePushService.getById(id);

        return Result.ok().put("messagePush", messagePush);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody MessagePushEntity messagePush){
		messagePushService.save(messagePush);

        return Result.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody MessagePushEntity messagePush){
		messagePushService.updateById(messagePush);

        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody String[] ids){
		messagePushService.removeByIds(Arrays.asList(ids));

        return Result.ok();
    }

}
