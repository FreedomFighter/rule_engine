package com.rule.front.api.drools;

import com.rule.core.utils.PageUtils;
import com.rule.core.utils.Result;
import com.rule.drools.service.EntityInfoService;
import com.rule.drools.vo.req.RulesEntityReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 规则引擎实体信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@RestController
@RequestMapping("rules/entityinfo")
public class EntityInfoController {
    @Autowired
    private EntityInfoService entityInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = entityInfoService.queryPage(params);
        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{entityId}")
    public Result info(@PathVariable("entityId") Long entityId){
        RulesEntityReqVo one = entityInfoService.findOne(entityId);
        return Result.ok().put("entityInfo", one);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody RulesEntityReqVo entityInfo){
        return entityInfoService.saveOrUpdateRuleEntityInfo(entityInfo);
    }

    /**
     * @Author gz
     * @Description 实体下拉列表
     * @Date 2019/7/15 15:54
     * @Param
     * @return Map<ID,Name></>
     */
    @RequestMapping("/listEntityName")
    public Result listEntityName(){
        List<Map<String, Object>> map = entityInfoService.listEntityName();
        return Result.ok().put("list",map);
    }

   /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("rules:entityinfo:update")
    public Result update(@RequestBody RulesEntityReqVo reqVo){
		entityInfoService.saveOrUpdateRuleEntityInfo(reqVo);
        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] entityIds){
		entityInfoService.removeByIds(Arrays.asList(entityIds));

        return Result.ok();
    }

}
