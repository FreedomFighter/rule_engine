package com.rule.front;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.rule")
@MapperScan(basePackages = "com.rule.*.dao")
@EnableMethodCache(basePackages = "com.rule")
@EnableCreateCacheAnnotation
public class FrontAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontAdminApplication.class, args);
    }
}
